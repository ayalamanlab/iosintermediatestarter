//
//  AppDelegate.swift
//  IOSIntermediateStarter
//
//  Created by Daniel Ayala Jabon on 16/12/2019.
//  Copyright © 2019 imediayala. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {



    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }
}

